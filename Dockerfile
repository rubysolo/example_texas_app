# ====== frontend build layer =================================================
FROM node:9 as frontend

WORKDIR /build
COPY . .

# we need to use elm-github-install since native modules are not officially supported
RUN cd assets \
    && npm install \
    && node_modules/.bin/brunch build --env production

# ====== backend build layer ==================================================
FROM bitwalker/alpine-elixir:1.7.3 as backend

WORKDIR /build
COPY . .
ENV MIX_ENV prod
RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get

# copy in frontend app
COPY --from=frontend /build/priv/static /build/priv/static/

# finalize and release
RUN mix phx.digest
RUN mix release --env=prod

# ====== service layer ========================================================
FROM alpine:3.7

RUN apk --no-cache add \
        bash \
        ncurses-libs \
        openssl

WORKDIR /app
COPY --from=backend /build/_build/prod/rel/working_on_docs .

ENV PORT 4000

CMD ["./bin/working_on_docs", "foreground"]